<?php
class Offer extends Model{
    
    public function create($parentId, $title, $description, $type, $qualification, $startDate, $finishDate){
        $sql = "INSERT INTO offers (idCompany, title, description, type, qualification, date, startDate, finishDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $req = Database::getBdd()->prepare($sql);
        @$req->bind_param("isssssss", $parentId, $title, $description, $type, $qualification, date('Y-m-d H:i:s'), $startDate, $finishDate);
        return $req->execute();
    }

    public function showOffer($idOffer){
        $sql = "SELECT * FROM offers WHERE offers.idOffer = ?";
        $req = Database::getBdd()->prepare($sql);
        @$req->bind_param("i", $idOffer);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllOffers($idCompany){
        $sql = "SELECT * FROM offers WHERE offers.idCompany = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("i", $idCompany);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showOffersList(){
        $sql = "SELECT * FROM offers";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function edit($idOffer, $title, $description, $type, $qualification, $startDate, $finishDate){
        $sql = "UPDATE offers SET title = ?, description = ?, type = ?, qualification = ?, startDate = ?, finishDate = ? WHERE idOffer = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ssssssi", $title, $description, $type, $qualification, $startDate, $finishDate, $idOffer);
        return $req->execute();
    }

    public function hide($idOffer, $isHidden){
        $sql = "UPDATE offers SET isHidden = ? WHERE idOffer = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ii", $isHidden, $idOffer);
        return $req->execute();
    }

    public function show($idOffer, $isHidden){
        $sql = "UPDATE offers SET isHidden = ? WHERE idOffer = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ii", $isHidden, $idOffer);
        return $req->execute();
    }

    public function delete($idOffer){
        $sql = 'DELETE FROM offers WHERE idOffer = ?';
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("i", $idOffer);
        return $req->execute();
    }

    public function deleteAll($id){
        $sql = 'DELETE FROM offers WHERE idCompany = ?';
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("i", $id);
        return $req->execute();
    }

}
