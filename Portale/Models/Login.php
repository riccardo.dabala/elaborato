<?php
class Login extends Model{

    public function register($username, $password){
        $passHash = md5($password);
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ss", $username, $passHash);
        return $req->execute();
    }

    public function check($username, $password){
        $passHash = md5($password);
        $sql = "SELECT * FROM users WHERE username = ? AND password = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ss", $username, $passHash);
        $req->execute();
        return  $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

}
?>