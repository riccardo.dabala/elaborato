<?php
class Company extends Model{

    public function create($businessName, $vat, $city, $address, $email, $tel){
        $sql = "INSERT INTO companies (businessName, vat, city, address, email, tel) VALUES (?, ?, ?, ?, ?, ?)";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ssssss", $businessName, $vat, $city, $address, $email, $tel);
        return $req->execute();
    }

    public function showCompany($idCompany){
        $sql = "SELECT idCompany, businessName, vat, city, address, email, tel FROM companies WHERE idCompany =".$idCompany;
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function showAllCompanies(){
        $sql = "SELECT companies.* FROM companies LEFT JOIN offers ON offers.idCompany = companies.idCompany";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    public function edit($idCompany, $businessName, $vat, $city, $address, $email, $tel){
        $sql = "UPDATE companies SET businessName = ?, vat = ?, city = ?, address = ?, email = ?, tel = ? WHERE idCompany = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("ssssssi", $businessName, $vat, $city, $address, $email, $tel, $idCompany);
        return $req->execute();
    }

    public function delete($idCompany){
        $sql = 'DELETE FROM companies WHERE idCompany = ?';
        $req = Database::getBdd()->prepare($sql);
        $req->bind_param("i", $idCompany);

        if ($req->execute()){
            require(ROOT . "Models/Offer.php");
            $offer = new Offer();
            return $offer->deleteAll($idCompany);
        }
        return false;
    }
}
