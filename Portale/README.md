# ESAME DI STATO DI ISTRUZIONE SECONDARIA SUPERIORE
## Indirizzo: ITIA – INFORMATICA E TELECOMUNICAZIONI

## ARTICOLAZIONE INFORMATICA

### TRACCIA PER L’ELABORATO

Il candidato (che potrà eventualmente avvalersi delle conoscenze e competenze maturate
attraverso esperienze di personali, attività organizzate nell’ambito dei percorsi per le
competenze trasversali organizzate dal consiglio di classe, ricerche e/o approfondimenti
proposti durante il periodo di didattica a distanza) sviluppi entro il 31 maggio 2021 il seguente
argomento.

#### Traccia

L’istituto tecnico Leonardo Da Vinci intende rinnovare completamente la propria rete scolastica, a
tale scopo ha attivato una linea di comunicazione in fibra ottica, che sarà utilizzata per la connessione
verso la rete internet della segreteria della scuola, delle aule, dotate di personal computer e lavagna
multimediale, e di alcuni laboratori utilizzati per le esercitazioni degli studenti.
Nel progetto complessivo, oltre alla realizzazione della rete, è previsto lo sviluppo di un portale di
servizi web che saranno distribuiti agli studenti, ai docenti e alle famiglie nella extranet della scuola.
Il portale sarà raggiungibile attraverso i computer dei laboratori e alcune aree WiFi opportunamente
attrezzate ed eventualmente la rete internet con i dovuti livelli di sicurezza.
Tra i servizi web che saranno realizzati vi è il servizio “Offerte di lavoro” dal quale le aziende che
hanno sviluppato con la scuola un accordo di collaborazione nell’ambito del PCTO potranno
pubblicare le offerte di lavoro e/o stage per i diplomati dell’istituto.
Per la registrazione dell’azienda sono sufficienti la ragione sociale, la partita IVA l’indirizzo, un
telefono e/o un indirizzo di posta per richiedere informazioni e, possibilmente, un logo Aziendale.
Per le offerte di lavoro un titolo una breve descrizione, la tipologia (assunzione a tempo determinato,
indeterminato, stage) la qualifica, la data di pubblicazione e il periodo di validità.
L’azienda potrà accedere ad apposita area riservata anche per rimuovere o rendere invisibile le offerte.

Il candidato, dopo aver formulato le necessarie ipotesi aggiuntive sviluppi almeno 3 dei 5 punti:

1. Fornisca una soluzione di massima per il progetto della rete scolastica studiando la topologia
logica della rete stessa e analizzando in modo particolare la programmazione dei switch e del
router per mantenere isolati i diversi laboratori.
2. Illustri quali accorgimenti sarà necessario adottare per la protezione dal malware dei singoli
dispositivi di rete presenti in istituto.
3. Proponga un progetto di massima della soluzione web necessaria a realizzare l’applicazione
“offerte di lavoro” individuando in particolare il modello concettuale e logico della base di
dati
4. Mostri una parte significativa del codice necessario allo sviluppo della soluzione web
utilizzando uno degli strumenti di sviluppo analizzati nel corso dell’anno scolastico.
5. Individui i tempi e le modalità di installazione della nuova rete riducendo al minimo i tempi
di interruzione del servizio o in alternativa pianifichi mediante la realizzazione di un WBS
opportunamente illustrato le fasi necessarie alla realizzazione dell’applicazione web per la
prenotazione.
Formato e consegna dell’elaborato
Il testo dell’elaborato viene consegnato come file in formato pdf usando il template fornito dal
Dipartimento di Informatica, esso dovrà occupare al massimo 10 cartelle dattiloscritte, possedere un
abstract di mezza pagina eventualmente anche in lingua inglese nonché, alla fine, una bibliografia
delle fonti (compresa eventuale sitografia e riferimenti ai materiali di progetto: repository, sito ecc.);
copertina, figure, diagrammi, abstract e bibliografia non rientrano nel conteggio del numero delle
cartelle.
Per l’esposizione
Possono risultare utili una o due slide ed eventualmente un prototipo funzionante anche parzialmente:
al candidato saranno lasciati circa 5’ per iniziare il colloquio usando suoi materiali.