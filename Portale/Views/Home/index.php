<head>
    <title>Portale Web - Home</title>
</head>

<body>

    <style>
        body {
            padding-top: 14rem;
        }

        .container {
            padding-right: 120px;
            padding-left: 120px;
        }
    </style>

    <div class="container">
        <div class="card" style="background-color:#202020; color:white">
            <div class="card-body">

                <h1 class="card-title">Benvenuto!</h1>
                In questo sito potrai trovare le offerte di lavoro più adatte a te. <br>
                <br>
                <a href='/Portale/offers/list/' class='btn btn-primary btn-xs'> Entra nel sito</a>

            </div>
        </div>
    </div>

</body>