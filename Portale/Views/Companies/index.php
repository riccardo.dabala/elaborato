<head>
    <title>Portale Web - Aziende</title>
</head>

<body>

    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Aziende</h1>
            <br>
            <table class="table table-striped table-dark table-bordered table-hover" style="background-color:#202020; color:white">
                <thead>
                    <tr>
                        <th>Ragione Sociale</th>
                        <th>Partita IVA</th>
                        <th>Città</th>
                        <th>Indirizzo</th>
                        <th>E-mail</th>
                        <th>Telefono</th>
                        <th>Options:</th>
                    </tr>
                </thead>
                <?php



                foreach ($companies as $company) {
                    echo '<tr>';
                    echo "<td>" . $company[0]['businessName'] . "</td>";
                    echo "<td>" . $company[0]['vat'] . "</td>";
                    echo "<td>" . $company[0]['city'] . "</td>";
                    echo "<td>" . $company[0]['address'] . "</td>";
                    echo "<td>" . $company[0]['email'] . "</td>";
                    echo "<td>" . $company[0]['tel'] . "</td>";

                    echo "<td class='text-center'>
                            <a class='btn btn-success btn-xs' href='/Portale/companies/edit/" . $company[0]["idCompany"] . "' >
                            <span class='glyphicon glyphicon-edit'></span> Modifica</a> 
                            
                            <a class='btn btn-danger btn-xs' href='/Portale/companies/delete/" . $company[0]["idCompany"] . "'>
                            <span class='glyphicon glyphicon-remove'></span> Elimina</a>
                            
                            <a class='btn btn-primary btn-xs' href='/Portale/offers/index/" . $company[0]["idCompany"] . "'>
                            <span class='glyphicon glyphicon-add'></span> Offerte...</a>
                        </td>";

                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>
    <br>
    <div>
        <a href="/Portale/companies/create/" class="btn btn-primary btn-xs pull-right">Nuova Azienda <b>+</b></a>
    </div>