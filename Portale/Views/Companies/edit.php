<head>
    <title>Portale Web - Modifica info</title>
</head>

<body>
    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Modifica le informazioni dell'azienda</h1>
            <br>
            <form method='post' action='#'>
                <div class="form-group">
                    <label for="businessName">Ragione sociale*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="businessName" placeholder="Inserisci la ragione sociale..." name="businessName" value="<?php if (isset($company[0]["businessName"])) echo $company[0]["businessName"]; ?>">
                </div>

                <div class="form-group">
                    <label for="vat">Partita IVA*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="vat" placeholder="Inserisci la partita IVA..." name="vat" value="<?php if (isset($company[0]["vat"])) echo $company[0]["vat"]; ?>">
                </div>

                <div class="form-group">
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="date1">Città*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="city" name="city" value="<?php if (isset($company[0]["city"])) echo $company[0]["city"]; ?>">
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="address">Indirizzo*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="address" name="address" value="<?php if (isset($company[0]["address"])) echo $company[0]["address"]; ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="contactInfo">Informazioni di contatto:</label>
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="email">E-mail*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="email" name="email" value="<?php if (isset($company[0]["email"])) echo $company[0]["email"]; ?>">
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="tel">Numero di telefono*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="tel" name="tel" value="<?php if (isset($company[0]["tel"])) echo $company[0]["tel"]; ?>">
                        </div>
                    </div>
                </div>

                <i><strong>
                        <p style="color:red; text-align:right">*Campo obbligatorio.</p>
                    </strong></i>

                <a class='btn btn-danger btn-xs' href='/Portale/companies/index/'><span class='glyphicon glyphicon-cancel'></span> Annulla</a>
                <button type="submit" class="btn btn-success">Salva</button>
            </form>

        </div>
    </div>

</body>