<?php
$titleErr = $descriptionErr = $typeErr = $addressErr = $numberErr = $emailErr = $ageErr = "";

if (empty($_POST['title'])) {
    $titleErr = "Campo obbligatorio";
}

if (empty($_POST['description'])) {
    $descriptionErr = "Campo obbligatorio";
}

if (empty($_POST['type'])) {
    $typeErr = "Campo obbligatorio";
}

if (empty($_POST['address'])) {
    $addressErr = "Campo obbligatorio*";
}

if (empty($_POST['number'])) {
    $numberErr = "Campo obbligatorio*";
} elseif (!is_numeric($_POST['number'])) {
    $numberErr = "Caratteri non validi";
}

if (empty($_POST['email'])) {
    $emailErr = "Campo obbligatorio*";
} elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Formato email non valido.";
}

if (empty($_POST['age'])) {
    $ageErrErr = "Campo obbligatorio*";
} elseif (!is_numeric($_POST['age'])) {
    $ageErr = "Caratteri non validi";
}
?>

<!----------------------------------------------------->

<head>
    <title>Portale Web - Registrazione azienda</title>
</head>

<body>

    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Registrazione azienda</h1>
            <br>
            <form method='post' action='#'>
                <div class="form-group">
                    <label for="businessName">Ragione sociale*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="businessName" placeholder="Inserisci la ragione sociale..." name="businessName">
                </div>

                <div class="form-group">
                    <label for="vat">Partita IVA*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="vat" placeholder="Inserisci la partita IVA..." name="vat">
                </div>

                <div class="form-group">
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="date1">Città*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="date1" placeholder="Inserisci la città..." name="city">
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="address">Indirizzo*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="address" placeholder="Inserisci l'indirizzo..." name="address">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="contactInfo">Informazioni di contatto:</label>
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="email">E-mail*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="email" placeholder="abc@example.com" name="email">
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="tel">Numero di telefono*</label>
                            <input style="background-color:#202020; color:white" type="text" class="form-control" id="tel" placeholder="Inserisci il numero di telefono..." name="tel">
                        </div>
                    </div>
                </div>

                <i><strong>
                        <p style="color:red; text-align:right">*Campo obbligatorio.</p>
                    </strong></i>

                <a class='btn btn-danger btn-xs' href='/Portale/companies/index/'><span class='glyphicon glyphicon-cancel'></span> Annulla</a>
                <button type="submit" class="btn btn-success">OK</button>
            </form>

        </div>
    </div>

</body>