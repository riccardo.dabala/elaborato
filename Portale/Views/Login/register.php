<head>
    <title>Portale Web - Register</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }
</style>

<div class="container">
    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Crea il tuo account</h1>

            <?php
            if (isset($_SESSION["errorMessage"])) {
            ?>
                <div class="error-message"><font color='red'><?php echo $_SESSION["errorMessage"]; ?></font></div>
            <?php
                unset($_SESSION["errorMessage"]);
            }
            ?>

            <form method='post' action='#' onSubmit = "return validate();">
                <div class="col-sm-6 col-sm offset-3">
                    <div class="form-group">
                        <label for="username">Username*</label> <span id="user" class="error-info"></span>
                        <input style="background-color:#202020; color:white" type="text" class="form-control" id="username" placeholder="Inserisci l'username..." name="username">
                    </div>
                    <div class="form-group">
                        <label for="password1">Password*</label> <span id="pass1" class="error-info"></span>
                        <input style="background-color:#202020; color:white" type="password" class="form-control" id="password1" placeholder="Inserisci la password..." name="password1">
                    </div>
                    <div class="form-group">
                        <label for="password2">Conferma Password*</label> <span id="pass2" class="error-info"></span>
                        <input style="background-color:#202020; color:white" type="password" class="form-control" id="password2" placeholder="Inserisci la password..." name="password2">
                    </div>
                </div>

                <i>
                    <strong>
                        <p style="color:red; text-align:right">*Campo obbligatorio.</p>
                    </strong>
                </i>

                <a class='btn btn-danger btn-xs' href='/Portale/login/index/'><span class='glyphicon glyphicon-cancel'></span> Indietro</a>
                <button type="submit" class="btn btn-success">Registrati</button>
            </form>

            <script>
                function validate() {
                    var $valid = true;
                    document.getElementById("user").innerHTML = "";
                    document.getElementById("pass1").innerHTML = "";
                    document.getElementById("pass2").innerHTML = "";

                    var username = document.getElementById("username").value;
                    var password1 = document.getElementById("password1").value;
                    var password2 = document.getElementById("password2").value;
                    if (username == "") {
                        document.getElementById("user").innerHTML = "<font color='red'> <br>Username mancante</font>";
                        $valid = false;
                    }
                    if (password1 == "") {
                        document.getElementById("pass1").innerHTML = "<font color='red'> <br>Password mancante</font>";
                        $valid = false;
                    }
                    if (password2 == "") {
                        document.getElementById("pass2").innerHTML = "<font color='red'> <br>Conferma della password mancante</font>";
                        $valid = false;
                    }
                    return $valid;
                }
            </script>
        </div>
    </div>
</div>