<head>
    <title>Portale Web - Login</title>
</head>

<style>
    .container {
        padding-right: 100px;
        padding-left: 100px;
    }

    .form-group {
        text-align: left;
    }
</style>

<body>
<div class="container">
    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Accedi all'Area Personale</h1>

            <?php
            if (isset($_SESSION["errorMessage"])) {
            ?>
                <div class="error-message"><font color='red'><?php echo $_SESSION["errorMessage"]; ?></font></div>
            <?php
                unset($_SESSION["errorMessage"]);
            }
            ?>

            <form method='post' action='#' onSubmit="return validate();">
                <div class="col-sm-6 col-sm offset-3">
                    <div class="form-group">
                        <label for="username"></label><span id="user" class="error-info"></span>
                        <input style="background-color:#202020; color:white" type="text" class="form-control" id="username" placeholder="Inserisci l'username..." name="username">
                    
                        <label for="password"></label><span id="pass" class="error-info"></span>
                        <input style="background-color:#202020; color:white" type="password" class="form-control" id="password" placeholder="Inserisci la password..." name="password">
                    </div>
                </div>

                <a class='btn btn-danger btn-xs' href='/Portale/offers/list'><span class='glyphicon glyphicon-cancel'></span> Annulla</a>
                <button type="submit" class="btn btn-success">Login</button>
            </form>

            <script>
                function validate() {
                    var $valid = true;
                    document.getElementById("user").innerHTML = "";
                    document.getElementById("pass").innerHTML = "";

                    var username = document.getElementById("username").value;
                    var password = document.getElementById("password").value;
                    if (username == "") {
                        document.getElementById("user").innerHTML = "<font color='red'> <br>Username mancante</font>";
                        $valid = false;
                    }
                    if (password == "") {
                        document.getElementById("pass").innerHTML = "<font color='red'> <br>Password mancante</font>";
                        $valid = false;
                    }
                    return $valid;
                }
            </script>


            <br>Non sei registrato? <a href="/Portale/login/register">Registrati ora.</a>
        </div>
    </div>
</div>

</body>