<!doctype html>

<head>
  <meta charset="utf-8">

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet">

  <link href="starter-template.css" rel="stylesheet">

  <link href="https://i.imgur.com/VX39JEc.png" rel="icon">

  <!--<img style="max-width:40px; position:relative" src="https://i.imgur.com/m7tO3u1.png"></img>-->

  <style>
    body {
      background-color: #252525;
      background-image: url("https://i.imgur.com/GIs6vaP.png");
      background-repeat: no-repeat;
      background-blend-mode: soft-light;
      background-size: cover;
      padding-top: 5rem;
    }

    .starter-template {
      padding: 3rem 1.5rem;
      text-align: center;
    }

    .navbar-custom {
      background-color: #4433cc;
    }

    .footer {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      background-color: #202020;
      color: #505050;
      text-align: center;

    }

    .form-control {
      background-color: #202020;
      color: white
    }
  </style>
</head>

<body style="color:white">

  <?php
  if ($_SERVER["REQUEST_URI"] != "/Portale/home/index/") {
  ?>

    <nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
      <a class="navbar-brand" href="https://www.isisleonardodavinci.edu.it/"><img style="max-width:120px; position:relative" src="https://img.gothru.org/535/5093633715941938201/overlay/assets/20201118032809.3nVwsI.png?save=optimize"></img></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href='/Portale/home/index/'>
              <img style="max-width: 20px; margin-top: -4px; position:relative" src="https://www.ctapomezia.it/images/homew.png">
              Home <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href='/Portale/offers/list/'>Offerte di lavoro</a>
          </li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item active navbar-right">
            <a class="nav-link" href="/Portale/login/index/">Area Personale</a>
          </li>
        </ul>
      </div>
    </nav>

  <?php
  }
  ?>
  <main role="main" class="container">

    <div class="starter-template">

      <?php
      echo $content_for_layout;
      ?>

    </div>

  </main>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

</body>

</html>

<div class=footer>
  Copyright © 2021 Riccardo Dabalà. All rights reserved.
</div>