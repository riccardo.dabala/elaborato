<head>
    <title>Portale Web - </title>
</head>

<body>

    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Offerte dell'Azienda</h1>
            <br>
            <table class="table table-striped table-dark table-bordered table-hover" style="background-color:#202020; color:white">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Options:</th>
                    </tr>
                </thead>
                <?php
                foreach ($offers as $offer) {
                    echo "<tr>";
                    echo "<td> <b>" . $offer['idOffer'] . "</b></td>";
                    echo "<td>" . $offer['title'] . "</td>";
                    echo "<td>" . $offer['description'] . "</td>";
                    echo "<td class='text-center'><a class='btn btn-success btn-xs' href='/Portale/offers/edit/" . $offer["idOffer"] . "' >
                        <span class='glyphicon glyphicon-edit'></span> Modifica</a>"; 
                        
                        if($offer['isHidden'] == false){
                            echo " <a href='/Portale/offers/hide/" . $offer["idOffer"] . "' class='btn btn-primary btn-xs'>
                            <span class='glyphicon glyphicon-remove'></span>  Nascondi "; 
                        } else {
                            echo " <a href='/Portale/offers/show/" . $offer["idOffer"] . "' class='btn btn-primary btn-xs'>
                            <span class='glyphicon glyphicon-remove'></span>  Mostra "; 
                        }
                        
                        
                    echo "</a>
                        <a href='/Portale/offers/delete/" . $offer["idOffer"] . "' class='btn btn-danger btn-xs'>
                        <span class='glyphicon glyphicon-remove'></span> Elimina</a></td>";

                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>
    <br>
    <div>
        <a class='btn btn-danger btn-xs' href='/Portale/companies/index/'><span class='glyphicon glyphicon-cancel'></span> Indietro</a>
        <a href="/Portale/offers/create/<?php echo $parentId ?>" class="btn btn-primary btn-xs pull-right">Nuova Offerta <b>+</b></a>
    </div>