<?php
$titleErr = $descriptionErr = $typeErr = $addressErr = $numberErr = $emailErr = $ageErr = "";

if (empty($_POST['title'])) {
    $titleErr = "Campo obbligatorio";
}

if (empty($_POST['description'])) {
    $descriptionErr = "Campo obbligatorio";
}

if (empty($_POST['type'])) {
    $typeErr = "Campo obbligatorio";
}

if (empty($_POST['address'])) {
    $addressErr = "Campo obbligatorio*";
}

if (empty($_POST['number'])) {
    $numberErr = "Campo obbligatorio*";
} elseif (!is_numeric($_POST['number'])) {
    $numberErr = "Caratteri non validi";
}

if (empty($_POST['email'])) {
    $emailErr = "Campo obbligatorio*";
} elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Formato email non valido.";
}

if (empty($_POST['age'])) {
    $ageErrErr = "Campo obbligatorio*";
} elseif (!is_numeric($_POST['age'])) {
    $ageErr = "Caratteri non validi";
}
?>

<!----------------------------------------------------->

<head>
    <title>Portale Web - Nuova Offerta</title>
</head>

<body>
    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-text">Nuova offerta</h1>
            <br>
            <form method='post' action='#'>
                <div class="form-group">
                    <label for="title">Titolo*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="title" placeholder="Inserisci un titolo..." name="title">
                </div>

                <div class="form-group">
                    <label for="description">Descrizione*</label>
                    <textarea style="background-color:#202020; color:white" rows="3" class="form-control" id="description" placeholder="Inserisci una descrizione..." name="description"></textarea>
                </div>

                <label for="type">Tipologia*</label>
                <div class="form-group">
                    
                    <label class="btn btn-primary" for="det">
                        <input type="radio" class="btn-check" id="det" autocomplete="off" value="Tempo Determinato" name="type"> Tempo Determinato
                    </label>

                    <label class="btn btn-primary" for="ind">
                        <input type="radio" class="btn-check" id="ind" autocomplete="off" value="Tempo Indeterminato" name="type"> Tempo Indeterminato
                    </label>

                    <label class="btn btn-primary" for="stg">
                        <input type="radio" class="btn-check" id="stg" autocomplete="off" value="Stage" name="type"> Stage
                    </label>
                </div>

                <div class="form-group">
                    <label for="qualification">Qualifica*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="qualification" placeholder="Inserisci una qualifica..." name="qualification">
                </div>

                <div class="form-group">
                    <label for="period">Periodo di validità*</label>
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="startDate">Data di inizio:</label>
                            <input style="background-color:#202020; color:white" type="date" class="form-control" id="startDate" placeholder="gg/mm/aaa" name="startDate">
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="finishDate">Data di termine:</label>
                            <input style="background-color:#202020; color:white" type="date" class="form-control" id="finishDate" placeholder="gg/mm/aaa" name="finishDate">
                        </div>
                    </div>
                </div>

                <i><strong>
                        <p style="color:red; text-align:right">*Campo obbligatorio.</p>
                    </strong></i>

                <a class='btn btn-danger btn-xs' href='/Portale/companies/index/'><span class='glyphicon glyphicon-cancel'></span> Annulla</a>
                <button type="submit" class="btn btn-success">OK</button>
            </form>
        </div>
    </div>
</body>