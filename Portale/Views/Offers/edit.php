<head>
    <title>Portale Web - Modifica Offerta</title>
</head>

<body>

    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Modifica offerta</h1>
            <br>
            <form method='post' action='#'>
                <div class="form-group">
                    <label for="title">Titolo*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="title" placeholder="Inserisci un titolo..." name="title" value="<?php if (isset($offer[0]["title"])) echo $offer[0]["title"]; ?>">
                </div>

                <div class="form-group">
                    <label for="description">Descrizione*</label>
                    <textarea style="background-color:#202020; color:white" rows="3" class="form-control" id="description" placeholder="Inserisci una descrizione..." name="description"><?php if (isset($offer[0]["description"])) echo $offer[0]["description"]; ?></textarea>
                </div>

                <label for="type">Tipologia*</label>
                <div class="form-group">
                    <label class="btn btn-primary" for="det">
                        <input type="radio" class="btn-check" id="det" autocomplete="off" name="type" value="Tempo Determinato"> Tempo Determinato
                    </label>

                    <label class="btn btn-primary" for="ind">
                        <input type="radio" class="btn-check" id="ind" autocomplete="off" name="type" value="Tempo Indeterminato"> Tempo Indeterminato
                    </label>

                    <label class="btn btn-primary" for="stg">
                        <input type="radio" class="btn-check" id="stg" autocomplete="off" name="type" value="Stage"> Stage
                    </label>
                </div>

                <div class="form-group">
                    <label for="qualification">Qualifica*</label>
                    <input style="background-color:#202020; color:white" type="text" class="form-control" id="qualification" value="<?php if (isset($offer[0]["qualification"])) echo $offer[0]["qualification"]; ?>"name="qualification">
                </div>

                <div class="form-group">
                    <label for="period">Periodo di validità*</label>
                    <div class="form-group row">
                        <div class="col-6 col-form-label">
                            <label for="startDate">Data di inizio:</label>
                            <input style="background-color:#202020; color:white" type="date" class="form-control" id="startDate" value="<?php if (isset($offer[0]["startDate"])) echo $offer[0]["startDate"]; ?>" name="startDate"> 
                        </div>
                        <div class="col-6 col-form-label">
                            <label for="finishDate">Data di termine:</label>
                            <input style="background-color:#202020; color:white" type="date" class="form-control" id="finishDate" value="<?php if (isset($offer[0]["finishDate"])) echo $offer[0]["finishDate"]; ?>" name="finishDate">
                        </div>
                    </div>
                </div>

                <i><strong>
                        <p style="color:red; text-align:right">*Campo obbligatorio.</p>
                    </strong></i>

                <a class='btn btn-danger btn-xs' href="/Portale/companies/index/"<span class='glyphicon glyphicon-cancel'></span> Annulla</a>
                <button type="submit" class="btn btn-success">Salva</button>
            </form>

        </div>
    </div>