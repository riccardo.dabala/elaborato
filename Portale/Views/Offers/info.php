<head>
    <title>Portale Web - Info</title>
</head>

<body>

    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title"><?php echo $offer[0]['title']; ?></h1>
            <p class="card-text">
                <?php echo $offer[0]['description']; ?>
            </p>

            <table class="table table-striped table-dark table-bordered table-hover" style="background-color:#202020; color:white">
                <tbody>

                    <tr>
                        <td>
                            Tipologia
                        </td>
                        <td colspan="2">
                            <?php echo $offer[0]['type']?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Qualifica
                        </td>
                        <td colspan="2">
                            <?php echo $offer[0]['qualification']; ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Pubblicata il giorno:
                        </td>
                        <td colspan="2">
                            <?php echo $offer[0]['date']; ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Periodo di validità
                        </td>
                        <td>
                            <?php echo $offer[0]['startDate']; ?>
                        </td>
                        <td>
                            <?php echo $offer[0]['finishDate']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>

    <a class='btn btn-danger btn-xs' href='/Portale/offers/list/'><span class='glyphicon glyphicon-cancel'></span> Indietro</a>
</body>