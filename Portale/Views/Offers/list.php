<head>
    <title>Portale Web - Offerte</title>
</head>

<body>
    <div class="card" style="background-color:#202020; color:white">
        <div class="card-body">
            <h1 class="card-title">Offerte di Lavoro</h1>
            <br>
            <table class="table table-striped table-dark table-bordered table-hover" style="background-color:#202020; color:white">
                <thead>
                    <tr>
                        <th>Titolo</th>
                        <th>Tipologia</th>
                        <th>Opzioni:</th>
                    </tr>
                </thead>
                <?php
                foreach ($offers as $offer) {
                    if ($offer['isHidden'] != true) {
                        echo "<tr>";
                        echo "<td>" . $offer['title'] . "</td>";
                        echo "<td>" . $offer['type'] . "</td>";
                        echo "<td>
                    <a 
                        href='/Portale/offers/info/" . $offer["idOffer"] . "' class='btn btn-primary btn-xs'> Mostra info...
                    </a>
                    </td>";
                        echo "</tr>";
                        echo "</tr>";
                    }
                }
                ?>
            </table>
        </div>
    </div>