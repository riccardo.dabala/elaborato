<?php

class offersController extends Controller{

    function index($id){
        if(!empty($id)){
            require(ROOT.'Models/Offer.php');
            $offer = new Offer();
            
            $d["offers"] = $offer->showAllOffers($id);
            $d["parentId"] = $id;

            $this->set($d);

            $this->render("index");
        } else {
            header("Location: ".WEBROOT."companies/index");
        }
    }

    function list(){
        require(ROOT . 'Models/Offer.php');
        $offer = new Offer();

        $d["offers"] = $offer->showOffersList();
        $this->set($d);
        $this->render("list");
    }

    function info($id){
        require(ROOT.'Models/Offer.php');
        $offer = new Offer();

        $d["offer"] = $offer->showOffer($id);
        $this->set($d);
        $this->render("info");
    }

    static function group_by($key, $data){
        $result = array();
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        return $result;
    }

    function create($parentId){
        if (isset($_POST["title"]) && isset($parentId)){
            require(ROOT.'Models/Offer.php');
            $offer= new Offer();
        
            if ($offer->create($parentId, $_POST["title"], $_POST["description"], $_POST["type"], $_POST["qualification"], $_POST["startDate"], $_POST["finishDate"])){
                header("Location: ".WEBROOT."offers/index/".$parentId);
            }
        }
        $this->render("create");
    }

    function edit($id){
        require(ROOT.'Models/Offer.php');
        $offer = new Offer();

        $d["offer"] = $offer->showOffer($id);

        if (isset($_POST["title"]) && isset($id)){
            if ($offer->edit($id, $_POST["title"], $_POST["description"], $_POST["type"], $_POST["qualification"], $_POST["startDate"], $_POST["finishDate"])){
                header("Location: ".WEBROOT."companies/index");
            }
        }

        $this->set($d);
        $this->render("edit");
    }

    function hide($id){
        require(ROOT.'Models/Offer.php');
        $offer = new Offer();

        
        if($offer->hide($id, 1)){
            header("Location: ".WEBROOT."companies/index");
        }
    }

    function show($id){
        require(ROOT.'Models/Offer.php');
        $offer = new Offer();

        if($offer->show($id, 0)){
            header("Location: ".WEBROOT."companies/index");
        }
    }

    function delete($id){
        require(ROOT.'Models/Offer.php');
        $offer = new Offer();

        if ($offer->delete($id)){
            header("Location: ".WEBROOT."companies/index");
        }
    }
    
}
