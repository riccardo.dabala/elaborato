<?php
class loginController extends Controller{

    function index(){
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            require(ROOT . 'Models/Login.php');
            $user = new Login();

            if ($user->check($_POST["username"], $_POST["password"]) && $_POST["username"] != null && $_POST["password"] != null) {
                session_start();
                header("Location: ".WEBROOT."companies/index/");
            } else {
                $_SESSION["errorMessage"] = "Username o password non corretti";
            }
        }

        $this->render("index");
    }

    function register(){
        if (isset($_POST["username"]) && $_POST["password1"] == $_POST["password2"]) {
            require(ROOT . 'Models/Login.php');
            $user = new Login();
            
            if ($user->register($_POST["username"], $_POST["password1"])) {
                header("Location: ".WEBROOT."login/index/");
            }
        } else if(isset($_POST["username"]) && $_POST["password1"] != $_POST["password2"]){
            $_SESSION["errorMessage"] = "La password di conferma non corrisponde";
        }

        $this->render("register");
    }
}
?>
