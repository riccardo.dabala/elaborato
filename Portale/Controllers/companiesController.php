<?php
class companiesController extends Controller{
    
    function index(){
        require(ROOT . 'Models/Company.php');
        $company = new Company();

        $d['companies'] = $this->group_by("idCompany", $company->showAllCompanies());
        $this->set($d);
        $this->render("index");
    }

    static function group_by($key, $data){
        $result = array();
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        return $result;
    }

    function create(){
        if (isset($_POST["businessName"]))
        {
            require(ROOT.'Models/Company.php');
            $company= new Company();

            if ($company->create($_POST["businessName"], $_POST["vat"], $_POST["city"], $_POST["address"], $_POST["email"], $_POST["tel"])){
                header("Location: ".WEBROOT."companies/index");
            }
        }

        $this->render("create");
    }

    function edit($idCompany){
        require(ROOT.'Models/Company.php');
        $company= new Company();

        $d["company"] = $company->showCompany($idCompany);

        if (isset($_POST["businessName"])){
            if ($company->edit($idCompany, $_POST["businessName"], $_POST["vat"], $_POST["city"], $_POST["address"], $_POST["email"], $_POST["tel"])){
                header("Location: ".WEBROOT."companies/index");
            }
        }
        $this->set($d);
        $this->render("edit");
    }

    function delete($idCompany){
        require(ROOT.'Models/Company.php');
        $company = new Company();

        if ($company->delete($idCompany)){
            header("Location: ".WEBROOT."companies/index");
        }
    }

    
}
?>