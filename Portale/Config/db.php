<?php

class Database{

    private static $bdd = null;

    private function __construct() {}

    public static function getBdd() {
        if(is_null(self::$bdd)) {
            self::$bdd = new mysqli("localhost", "root", "", "aziende");
        }
        return self::$bdd;
    }
}
?>