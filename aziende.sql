-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 24, 2021 alle 08:58
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aziende`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `companies`
--

CREATE TABLE `companies` (
  `idCompany` int(16) NOT NULL,
  `businessName` varchar(32) NOT NULL,
  `vat` varchar(11) NOT NULL,
  `city` varchar(16) NOT NULL,
  `address` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `companies`
--

INSERT INTO `companies` (`idCompany`, `businessName`, `vat`, `city`, `address`, `email`, `tel`, `idUser`) VALUES
(1, 'a', 'a', 'a', 'a', 'a', 'a', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `offers`
--

CREATE TABLE `offers` (
  `idOffer` int(16) NOT NULL,
  `idCompany` int(16) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` varchar(128) NOT NULL,
  `type` varchar(64) NOT NULL,
  `qualification` varchar(64) NOT NULL,
  `date` date NOT NULL,
  `startDate` date NOT NULL,
  `finishDate` date NOT NULL,
  `isHidden` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `offers`
--

INSERT INTO `offers` (`idOffer`, `idCompany`, `title`, `description`, `type`, `qualification`, `date`, `startDate`, `finishDate`, `isHidden`) VALUES
(3, 1, 'te', 'mtem', 'Tempo Indeterminato', 'tem', '2021-05-24', '2021-05-19', '2021-05-20', 0),
(4, 1, '', '', 'Tempo Determinato', '', '2021-05-24', '0000-00-00', '0000-00-00', 0),
(5, 1, '', '', 'Stage', '', '2021-05-24', '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`idCompany`);

--
-- Indici per le tabelle `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`idOffer`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `companies`
--
ALTER TABLE `companies`
  MODIFY `idCompany` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `offers`
--
ALTER TABLE `offers`
  MODIFY `idOffer` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
